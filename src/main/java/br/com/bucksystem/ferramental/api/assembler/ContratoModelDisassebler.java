package br.com.bucksystem.ferramental.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.ContratoNewModelInput;
import br.com.bucksystem.ferramental.domain.model.Contrato;

@Component
public class ContratoModelDisassebler {
	
	@Autowired
	private ModelMapper modelMapper;

	public Contrato toDomainObject(ContratoNewModelInput contratoInput) {
		return modelMapper.map(contratoInput, Contrato.class);
	}

	public void copyToDomainObject(ContratoNewModelInput contratoInput, Contrato contrato) {
		modelMapper.map(contratoInput, contrato);
	}
}