package br.com.bucksystem.ferramental.domain.repository;

import java.util.List;

import br.com.bucksystem.ferramental.domain.model.Contrato;

public interface ContratoRepositoryQuery {
	
	List<Contrato> find(String conteudopesquisa);
	
}
