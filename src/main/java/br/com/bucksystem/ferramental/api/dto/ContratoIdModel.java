package br.com.bucksystem.ferramental.api.dto;

import lombok.Data;

@Data
public class ContratoIdModel {

	private Long id;
    
}
