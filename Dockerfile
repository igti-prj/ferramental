# systembs-comum-contrato
FROM openjdk:12-alpine

LABEL maintainer='Rogerio Costa <rogerio.costa@bucksystem.com.br>'

ARG PROFILE
ARG ADDITIONAL_OPTS

ENV PROFILE=${PROFILE}
ENV ADDITIONAL_OPTS=${ADDITIONAL_OPTS}

WORKDIR /opt/spring-boot-ferramental

COPY /target/ferramental*.jar ferramental.jar

SHELL ["/bin//sh", "-c"]

EXPOSE 8080

CMD java ${ADDITIONAL_OPTS} -jar ferramental.jar --spring.profiles.active=${PROFILE}
