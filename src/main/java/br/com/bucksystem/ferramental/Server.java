package br.com.bucksystem.ferramental;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import br.com.bucksystem.ferramental.domain.repository.infra.CustomJpaRepositoryImpl;

@SpringBootApplication
@EntityScan("br.com.bucksystem.ferramental.domain.model")
@EnableJpaRepositories(basePackages = {"br.com.bucksystem.ferramental.domain.repository",
									   "br.com.bucksystem.ferramental.domain.service",
									   "br.com.bucksystem.ferramental.api",
									   "br.com.bucksystem.ferramental.core"},
					   repositoryBaseClass = CustomJpaRepositoryImpl.class)
public class Server {
	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(Server.class, args);
	}
}
