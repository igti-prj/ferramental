package br.com.bucksystem.ferramental.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bucksystem.ferramental.core.exception.EntidadeNaoEncontradaException;
import br.com.bucksystem.ferramental.domain.model.Contrato;
import br.com.bucksystem.ferramental.domain.repository.ContratoRepository;

@Service
public class CadastroContratoService {

	private static final String MSG_REGISTRO_EM_USO = "Contrato de código %d não pode ser removida, pois está em uso";

	private static final String MSG_REGISTRO_NAO_ENCONTRADO = "Não existe um cadastro de Contrato com código %d";

	private static final String MSG_REGISTRO_INATIVO = "Impossível fazer manutenção do registro Id [%d][TContratos], registro está Inativado.";

	@Autowired
	private ContratoRepository contratoRepository;

	public Contrato buscarOuFalharId(Long contratoId) {
		return contratoRepository.findById(contratoId).orElseThrow(
				() -> new EntidadeNaoEncontradaException(String.format(MSG_REGISTRO_NAO_ENCONTRADO, contratoId)));
	}

}
