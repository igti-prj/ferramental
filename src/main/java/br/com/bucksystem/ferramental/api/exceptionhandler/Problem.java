package br.com.bucksystem.ferramental.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@JsonInclude(Include.NON_NULL)
public class Problem {
	
	//RFC 7807 Padronização do Formato de Problemas.	
	private Integer status;
	private String type;
	private String title;
	private String detail;
	private String uiMessage;
	private OffsetDateTime timeStamp;
	
	private List<Campo> campos;
	
	@Getter
	@Builder
	public static class Campo {
		private String nome;
		private String uiMessage;
	}
	
}
