use bsferramental;

select * from contrato;
	select * from empresa;
		select * from contrato_empresa,empresa 
			where contrato_id = 1
                  and contrato_empresa.empresa_id = empresa.id;

select * from local_estoque; 
-- delete from local_estoque where id > 0;
insert into local_estoque (nome,cadastro,alteracao) values ("Depósito",current_timestamp(),current_timestamp());
insert into local_estoque (nome,cadastro,alteracao) values ("Almoxarifado",current_timestamp(),current_timestamp());
insert into local_estoque (nome,cadastro,alteracao) values ("EmTransito",current_timestamp(),current_timestamp());

select * from empresa_localestoque;
	insert into empresa_localestoque values (1,2);
	insert into empresa_localestoque values (1,3);
	insert into empresa_localestoque values (1,4);

select * from estoque;
	select * from item_estoque;
		select * from item_estoque_tipo;
        
select * from estoque, item_estoque, item_estoque_tipo,empresa
 where 	estoque.empresa_id = 1
        and estoque.empresa_id = empresa.id
		and estoque.item_estoque_id = item_estoque.id
		and item_estoque.item_estoque_tipo_id = item_estoque_tipo.id;
        
select * from estoque;  
insert into estoque 
(alteracao,ativo,cadastro,etiqueta,nome,qtdade,unidadeestoque,empresa_id,item_estoque_id)
values (current_timestamp(),1,current_timestamp(),'Placa 11', 'Placa 11', 10, 'UN', 1,1);
insert into estoque 
(alteracao,ativo,cadastro,etiqueta,nome,qtdade,unidadeestoque,empresa_id,item_estoque_id)
values (current_timestamp(),1,current_timestamp(),'Placa 2', 'Placa 2', 20, 'UN', 1,1);
insert into estoque 
(alteracao,ativo,cadastro,etiqueta,nome,qtdade,unidadeestoque,empresa_id,item_estoque_id)
values (current_timestamp(),1,current_timestamp(),'Placa 2', 'Placa 2', 20, 'UN', 1,1);