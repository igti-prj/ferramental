package br.com.bucksystem.ferramental.domain.model;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
@Entity
public class ItemEstoque {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false,length=30)
	private String nome;
		
	private boolean ativo = true;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Contrato contrato;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private ItemEstoqueTipo itemEstoqueTipo;	
		
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime cadastro;
	
	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime alteracao;

}