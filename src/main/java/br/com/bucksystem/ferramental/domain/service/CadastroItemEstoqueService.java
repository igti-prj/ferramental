package br.com.bucksystem.ferramental.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bucksystem.ferramental.core.exception.EntidadeNaoEncontradaException;
import br.com.bucksystem.ferramental.domain.model.ItemEstoque;
import br.com.bucksystem.ferramental.domain.repository.ItemEstoqueRepository;

@Service
public class CadastroItemEstoqueService {

	private static final String MSG_REGISTRO_EM_USO = "Contrato de código %d não pode ser removida, pois está em uso";

	private static final String MSG_REGISTRO_NAO_ENCONTRADO = "Não existe um cadastro de Contrato com código %d";

	private static final String MSG_REGISTRO_INATIVO = "Impossível fazer manutenção do registro Id [%d][TContratos], registro está Inativado.";

	@Autowired
	private ItemEstoqueRepository itemEstoqueRepository;

	public ItemEstoque buscarOuFalharId(Long itemEstoqueId) {
		return itemEstoqueRepository.findById(itemEstoqueId).orElseThrow(
				() -> new EntidadeNaoEncontradaException(String.format(MSG_REGISTRO_NAO_ENCONTRADO, itemEstoqueId)));
	}

}
