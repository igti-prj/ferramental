package br.com.bucksystem.ferramental.api.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class PatrimonioIdModel {

	@NotBlank
	private Long id;

}
