package br.com.bucksystem.ferramental.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.ItemEstoqueNewModel;
import br.com.bucksystem.ferramental.domain.model.ItemEstoque;

@Component
public class ItemEstoqueModelDisassebler {
	
	@Autowired
	private ModelMapper modelMapper;

	public ItemEstoque toDomainObject(ItemEstoqueNewModel itemestoqueInptu) {
		return modelMapper.map(itemestoqueInptu, ItemEstoque.class);
	}

	public void copyToDomainObject(ItemEstoqueNewModel itemestoqueInput, ItemEstoque itemestoque) {
		modelMapper.map(itemestoqueInput, itemestoque);
	}
}