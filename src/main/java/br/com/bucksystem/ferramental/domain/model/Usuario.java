package br.com.bucksystem.ferramental.domain.model;

import java.time.OffsetDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false,length=50,unique=true)
	private String nome;
	
	@Column(nullable=false,length=100)
	private String senha;
	
	@Column(columnDefinition = "boolean default 1")
    private boolean ativo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable( name = "usuario_contrato",
            	joinColumns = @JoinColumn(name = "usuario_id"),
            	inverseJoinColumns = @JoinColumn(name = "contrato_id"))
    private List<Contrato> contratos;
		
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime cadastro;
	
	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime alteracao;
	
}