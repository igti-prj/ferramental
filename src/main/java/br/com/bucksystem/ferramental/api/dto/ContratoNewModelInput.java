package br.com.bucksystem.ferramental.api.dto;

import java.time.OffsetDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ContratoNewModelInput {

	@NotBlank @Size(max=30)
	private String nome;
	@NotNull
	private OffsetDateTime dataContratoInicio;
    private boolean ativo;

}
