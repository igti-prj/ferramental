package br.com.bucksystem.ferramental.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bucksystem.ferramental.api.assembler.EstoqueModelAssembler;
import br.com.bucksystem.ferramental.api.assembler.EstoqueModelDisassebler;
import br.com.bucksystem.ferramental.api.dto.EstoqueModel;
import br.com.bucksystem.ferramental.api.dto.ItemEstoqueModel;
import br.com.bucksystem.ferramental.domain.repository.EstoqueRepository;
import br.com.bucksystem.ferramental.domain.service.CadastroEstoqueService;

@RestController
@RequestMapping("/api/v1-1/estoques")
public class EstoqueApi {

	@Autowired
	private EstoqueRepository estoqueRepository;
	
	@Autowired
	private CadastroEstoqueService cadastroEstoque;
	
	@Autowired
	private SmartValidator validator;
		
	@Autowired
	private EstoqueModelAssembler estoqueModelAssembler;
	
	@Autowired
	private EstoqueModelDisassebler estoqueModelDisassembler;
	
	@GetMapping(value="/all" )
	public List<EstoqueModel> listar() {
		return estoqueModelAssembler.toCollectionModel(estoqueRepository.findAll());
	}

	@GetMapping(value="/{id}" )
	public EstoqueModel buscarId(@Valid @PathVariable("id") Long pk ) {
		return estoqueModelAssembler.toModel(cadastroEstoque.buscarOuFalharId(pk));
	}
	
}