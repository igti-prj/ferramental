package br.com.bucksystem.ferramental.core.exception;

public class EntidadeComRegistroInativoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public EntidadeComRegistroInativoException(String mensagem) {
		super(mensagem);
	}
	
	public EntidadeComRegistroInativoException(String mensagem, Throwable causa) {
		super( mensagem, causa);
	}
}