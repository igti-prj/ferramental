package br.com.bucksystem.ferramental.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bucksystem.ferramental.core.exception.EntidadeNaoEncontradaException;
import br.com.bucksystem.ferramental.domain.model.Usuario;
import br.com.bucksystem.ferramental.domain.repository.UsuarioRepository;

@Service
public class CadastroUsuarioService {

	private static final String MSG_REGISTRO_EM_USO 
		= "Usuário de código %d não pode ser removida, pois está em uso";

	private static final String MSG_REGISTRO_NAO_ENCONTRADO 
		= "Não existe um cadastro de Usuário com código %d";

	private static final String MSG_LOGIN_NAO_ENCONTRADO 
		= "Não existe um cadastro de Usuário com o LOGIN %s";
	
	private static final String MSG_REGISTRO_INATIVO = "Impossível fazer manutenção do registro Id [%d][TUsuários], registro está Inativado.";

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario buscarOuFalharId(Long userId) {
		return usuarioRepository.findById(userId).orElseThrow(
				() -> new EntidadeNaoEncontradaException(String.format(MSG_REGISTRO_NAO_ENCONTRADO, userId)));
	}
	
	public Usuario buscarOuFalharNome(String nome) {
		return usuarioRepository.findByNome(nome).orElseThrow(
				() -> new EntidadeNaoEncontradaException(String.format(MSG_LOGIN_NAO_ENCONTRADO, nome.trim())));
	}

}