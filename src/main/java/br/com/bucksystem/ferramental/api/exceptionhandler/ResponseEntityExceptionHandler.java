package br.com.bucksystem.ferramental.api.exceptionhandler;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;

import br.com.bucksystem.ferramental.api.exceptionhandler.Problem.Campo;
import br.com.bucksystem.ferramental.api.exceptionhandler.Problem.ProblemBuilder;
import br.com.bucksystem.ferramental.core.exception.EntidadeEmUsoException;
import br.com.bucksystem.ferramental.core.exception.EntidadeNaoEncontradaException;
import br.com.bucksystem.ferramental.core.exception.NegocioException;

@ControllerAdvice
public class ResponseEntityExceptionHandler extends org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler{

	private static final String MENSAGEM_GENERICA_USUARIO_FINAL = "Ocorreu um erro interno inesperado no sistema. "
			+ "Tente novamente e se o problema persistir, entre em contato "
			+ "com o administrador do sistema.";
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

	    ProblemType problemType = ProblemType.DADOS_INVALIDOS;
	    String detalhe = "Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.";
	       
	    BindingResult bindResult = ex.getBindingResult();
	    List<Problem.Campo> problemCampos = extrairCampos(bindResult);
	    
	    Problem problem = createProblemBuilder(status, problemType, detalhe)
	    		.campos(problemCampos)
	    		.build();
	    
	    return handleExceptionInternal(ex, problem, headers, status, request);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleUncaught(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;		
		ProblemType problemType = ProblemType.ERRO_DE_SISTEMA;
		String detail = MENSAGEM_GENERICA_USUARIO_FINAL;
		//ex.printStackTrace();
		Problem problem = createProblemBuilder(status, problemType, detail).build();
		return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, 
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		//throw-exception-if-no-handler-found: true
		//add-mappings: false
		ProblemType problemType = ProblemType.RECURSO_NAO_ENCONTRADO;
		String detail = String.format(	"O recurso %s, que você tentou acessar, é inexistente.", 
										ex.getRequestURL());
		Problem problem = createProblemBuilder(status, problemType, detail)
				.uiMessage(MENSAGEM_GENERICA_USUARIO_FINAL)
				.build();	
		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		if (ex instanceof MethodArgumentTypeMismatchException) {
			return handleMethodArgumentTypeMismatch(
					(MethodArgumentTypeMismatchException) ex, headers, status, request);
		}
		return super.handleTypeMismatch(ex, headers, status, request);
	}
	
	private ResponseEntity<Object> handleMethodArgumentTypeMismatch(
			MethodArgumentTypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		ProblemType problemType = ProblemType.PARAMETRO_INVALIDO;
		String detail = String.format("O parâmetro de URL '%s' recebeu o valor '%s', "
									  + "que é de um tipo inválido. Corrija e informe um valor compatível com o tipo %s.",
									  ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());

		Problem problem = createProblemBuilder(status, problemType, detail)
				.uiMessage(MENSAGEM_GENERICA_USUARIO_FINAL)
				.build();
		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		//pom.xml org.apache.commons.commons-lang3
		//fail-on-ignored-properties: true
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		
		if ( rootCause instanceof InvalidFormatException ) {
			return handleInvalidFormatException( (InvalidFormatException)rootCause,
												 headers, 
												 status, 
												 request );	
		} else if (rootCause instanceof PropertyBindingException) {
			return handlePropertyBindingException(	(PropertyBindingException)rootCause, 
													headers, 
													status, 
													request); 
		} 
		Problem problem = createProblemBuilder(	HttpStatus.BAD_REQUEST, 
						  						ProblemType.MENSAGEN_NAO_COMPREENSIVEL, 
						  						"Corpo da requisção está inválido, verifique erro sintaxe." )
				.uiMessage(MENSAGEM_GENERICA_USUARIO_FINAL)
				.build();
		return super.handleExceptionInternal(	ex,
												problem, 
												new HttpHeaders(), 
												HttpStatus.BAD_REQUEST, 
												request);
	}
	
	private ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
	
		String path = joinPath(ex.getPath());
		
		String detalhe = String.format("A propriedade '%s' não existe. "
									   + "Corrija ou remova essa propriedade e tente novamente.", 
									   path);

		Problem problem = createProblemBuilder(	HttpStatus.BAD_REQUEST, 
												ProblemType.MENSAGEN_NAO_COMPREENSIVEL, 
												detalhe)
				.uiMessage(MENSAGEM_GENERICA_USUARIO_FINAL)
				.build();
		
		return handleExceptionInternal(ex, problem, headers, status, request);
	}
	
	private ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String path = joinPath(ex.getPath());
		
		String detalhes = 
				String.format("A propriedade '%s' recebeu um valor"
				+ " '%s', que é de um tipo inválido. Informe um valor compátivel com o tipo '%s'.", 
				path, ex.getValue(), ex.getTargetType().getSimpleName() );
		
		Problem problem = createProblemBuilder(	HttpStatus.BAD_REQUEST, 
												ProblemType.MENSAGEN_NAO_COMPREENSIVEL, 
												detalhes )
				.uiMessage(MENSAGEM_GENERICA_USUARIO_FINAL)
				.build();
		return super.handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	//////////////
	@ExceptionHandler(NegocioException.class)
	public ResponseEntity<?> handleNegocioExecptioni( NegocioException ex, WebRequest request) {
		
		//ex.printStackTrace();
		
		Problem problem = createProblemBuilder(	HttpStatus.BAD_REQUEST, 
												ProblemType.ERRO_NEGOCIO, 
												ex.getMessage() )
				.build();
		return super.handleExceptionInternal(	ex, problem, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);		
	}
	
	@ExceptionHandler(EntidadeNaoEncontradaException.class)
	public ResponseEntity<?> handleEntidadeNaoEncontradaExeption( EntidadeNaoEncontradaException ex, WebRequest request){
		
		Problem problem = createProblemBuilder(	HttpStatus.NOT_FOUND, 
												ProblemType.ENTIDADE_NAO_ENCONTRADA, 
												ex.getMessage() ).build();
		return super.handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.NOT_FOUND, request);		
	}
		
	@ExceptionHandler(EntidadeEmUsoException.class)
	public ResponseEntity<?> handleEntidadeEmUsoException( EntidadeEmUsoException ex, WebRequest request){
		Problem problem = createProblemBuilder(	HttpStatus.CONFLICT, 
												ProblemType.ENTIDADE_EM_USO, 
												ex.getMessage() ).build();
		return super.handleExceptionInternal(	ex, problem, new HttpHeaders(), 
												HttpStatus.CONFLICT, request);		
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
	
		//RFC 7807 Padronização do Formato de Problemas.
		if (body==null) {
			body = Problem.builder()
					.title(status.getReasonPhrase())
					.status(status.value())
					.build();	
		} else if ( body instanceof String ) {
			body = Problem.builder()
					.title((String)body)
					.status(status.value())
					.build();
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	private Problem.ProblemBuilder createProblemBuilder(HttpStatus status, 
														ProblemType problemType, 
														String detail ){
		return Problem.builder()
				.timeStamp(OffsetDateTime.now())
				.status(status.value())
				.type(problemType.getUri())
				.detail(detail)
				.title(problemType.getTitle());
	}
	
	private String joinPath(List<Reference> references) {
		return references.stream()
			.map(ref -> ref.getFieldName())
			.collect(Collectors.joining("."));
	}
	
	private List<Problem.Campo> extrairCampos(BindingResult bindResult) {
		List<Problem.Campo> problemCampos = bindResult.getFieldErrors().stream()
	    		.map( fieldError -> Problem.Campo.builder()
	    				.nome(fieldError.getField())
	    				.uiMessage(fieldError.getDefaultMessage())
	    				.build())
	    		.collect(Collectors.toList());
		return problemCampos;
	}

}