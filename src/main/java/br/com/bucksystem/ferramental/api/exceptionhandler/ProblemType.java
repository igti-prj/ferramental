package br.com.bucksystem.ferramental.api.exceptionhandler;

import lombok.Getter;

@Getter
//RFC 7807 Padronização do Formato de Problemas.
public enum ProblemType {

	DADOS_INVALIDOS("/dados-invalidos", "Dados inválidos"),
	ERRO_DE_SISTEMA("/erro-de-sistema", "Erro de sistema"),
	RECURSO_NAO_ENCONTRADO("/recurso-nao-encontrado","Mensagem de recurso/api não encontrado."),
	PARAMETRO_INVALIDO("/parametro-invalido","Mensagem de passagem de Parametro Inválido."),
	MENSAGEN_NAO_COMPREENSIVEL("/mensagem-nao-compreensivel","Mensagem não compreensivel."),
	ENTIDADE_NAO_ENCONTRADA("/entidade-nao-encontrada","Entidade não Encontrada."),
	ENTIDADE_EM_USO("/entidade-em-uso","Entidade em Uso."),
	ERRO_NEGOCIO("/erro-negocio", "Violação de regra de negócio");
	
	private String title;
	private String uri;
	
	private ProblemType( String path, String title ) {
		this.uri   = "http://www.bucksystem.com.br" + path;
		this.title = title; 
	}
	
}
