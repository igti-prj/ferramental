package br.com.bucksystem.ferramental.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bucksystem.ferramental.api.assembler.ContratoModelAssembler;
import br.com.bucksystem.ferramental.api.assembler.ContratoModelDisassebler;
import br.com.bucksystem.ferramental.api.dto.ContratoModel;
import br.com.bucksystem.ferramental.domain.repository.ContratoRepository;
import br.com.bucksystem.ferramental.domain.service.CadastroContratoService;

@RestController
@RequestMapping("/api/v1-1/contratos")
public class ContratoApi {

	@Autowired
	private ContratoRepository contratoRepository;
	
	@Autowired
	private CadastroContratoService cadastroContrato;
	
	@Autowired
	private SmartValidator validator;
		
	@Autowired
	private ContratoModelAssembler contratoModelAssembler;
	
	@Autowired
	private ContratoModelDisassebler contratoModelDisassembler;
	
	@GetMapping(value="/all" )
	public List<ContratoModel> listar() {
		return contratoModelAssembler.toCollectionModel(contratoRepository.findAll());
	}

	@GetMapping(value="/{id}" )
	public ContratoModel buscarId(@Valid @PathVariable("id") Long pk ) {
		return contratoModelAssembler.toModel(cadastroContrato.buscarOuFalharId(pk));
	}
	
	@GetMapping(value="/find")
	public List<ContratoModel> find(String conteudopesquisa) {
		return contratoModelAssembler.toCollectionModel(contratoRepository.find(conteudopesquisa));
	}
				
}