package br.com.bucksystem.ferramental.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.ItemEstoqueModel;
import br.com.bucksystem.ferramental.domain.model.ItemEstoque;

@Component
public class ItemEstoqueModelAssembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ItemEstoqueModel toModel(ItemEstoque itemestoque) {		
		return modelMapper.map(itemestoque, ItemEstoqueModel.class);
	}
	
	public List<ItemEstoqueModel> toCollectionModel(List<ItemEstoque> itensestoques) {		
		return itensestoques.stream()
				.map(itemestoque -> toModel(itemestoque))
				.collect(Collectors.toList());
	}

}