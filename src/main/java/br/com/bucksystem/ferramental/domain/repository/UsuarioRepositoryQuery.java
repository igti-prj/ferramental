package br.com.bucksystem.ferramental.domain.repository;

import java.util.List;

import br.com.bucksystem.ferramental.domain.model.Usuario;

public interface UsuarioRepositoryQuery {

	List<Usuario> find(String conteudopesquisa);
	
}