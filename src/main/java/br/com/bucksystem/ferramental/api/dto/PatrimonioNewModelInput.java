package br.com.bucksystem.ferramental.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class PatrimonioNewModelInput {

	@NotBlank @Size(max=30)
	private String nome;
	private boolean ativo = true;

}
