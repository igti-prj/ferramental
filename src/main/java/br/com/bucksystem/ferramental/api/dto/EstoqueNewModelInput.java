package br.com.bucksystem.ferramental.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class EstoqueNewModelInput {

	@NotBlank  @Size(max=40)
	private String nome;
	@NotBlank  @Size(max=30)
	private String etiqueta;
	private boolean ativo = true;

}
