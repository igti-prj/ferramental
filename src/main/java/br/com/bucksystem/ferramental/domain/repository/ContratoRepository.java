package br.com.bucksystem.ferramental.domain.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.bucksystem.ferramental.domain.model.Contrato;
import br.com.bucksystem.ferramental.domain.repository.infra.CustomJpaRepository;

@Repository   
public interface ContratoRepository 
	extends CustomJpaRepository<Contrato, Long>, ContratoRepositoryQuery { 

	Optional<Contrato> findByNome(String nome);
	
}