package br.com.bucksystem.ferramental.domain.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.bucksystem.ferramental.domain.model.Usuario;
import br.com.bucksystem.ferramental.domain.repository.infra.CustomJpaRepository;

@Repository   
public interface UsuarioRepository 
	extends CustomJpaRepository<Usuario, Long>, UsuarioRepositoryQuery { 

	Optional<Usuario> findByNome(String nome);
	
}