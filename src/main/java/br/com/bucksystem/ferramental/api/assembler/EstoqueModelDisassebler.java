package br.com.bucksystem.ferramental.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.EstoqueNewModel;
import br.com.bucksystem.ferramental.domain.model.Estoque;

@Component
public class EstoqueModelDisassebler {
	
	@Autowired
	private ModelMapper modelMapper;

	public Estoque toDomainObject(EstoqueNewModel estoqueInptu) {
		return modelMapper.map(estoqueInptu, Estoque.class);
	}

	public void copyToDomainObject(EstoqueNewModel estoqueInput, Estoque estoque) {
		modelMapper.map(estoqueInput, estoque);
	}
}