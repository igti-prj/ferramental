# Read Me First
The following was discovered as part of building this project:

* The JVM level was changed from '14' to '11', review the [JDK Version Range](https://github.com/spring-projects/spring-framework/wiki/Spring-Framework-Versions#jdk-version-range) on the wiki for more details.
* The original package name 'br.com.bucksystem.systembs-ferrramental' is invalid and this project uses 'br.com.bucksystem.systembsferrramental' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.13.RELEASE/maven-plugin/)

