package br.com.bucksystem.ferramental.core.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
//		Exemplo de tratar, caso o modelmapper nao resolva.	
//		modelMapper.createTypeMap(Moeda.class, MoedaModel.class)
//		.addMapping(Moeda::getId, MoedaModel::setNumeroPK);
		return new ModelMapper();
	}
	
}