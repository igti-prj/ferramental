package br.com.bucksystem.ferramental.api.dto;

import lombok.Data;

@Data
public class ItemEstoqueModel {

	private Long id;
	private String nome;
	private boolean ativo;
	private ContratoModel contrato;
	private ItemEstoqueTipoModel itemEstoqueTipo;	

}
