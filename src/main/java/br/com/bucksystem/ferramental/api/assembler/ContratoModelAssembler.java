package br.com.bucksystem.ferramental.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.ContratoModel;
import br.com.bucksystem.ferramental.domain.model.Contrato;

@Component
public class ContratoModelAssembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ContratoModel toModel(Contrato contrato) {		
		return modelMapper.map(contrato, ContratoModel.class);
	}
	
	public List<ContratoModel> toCollectionModel(List<Contrato> contratos) {		
		return contratos.stream()
				.map(contrato -> toModel(contrato))
				.collect(Collectors.toList());
	}

}