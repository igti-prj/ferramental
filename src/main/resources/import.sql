insert into usuario (nome,senha,ativo,cadastro,alteracao) values ("rogerio","senha",1,utc_timestamp,utc_timestamp), ("jose","senha",1,utc_timestamp,utc_timestamp), ("maria","senha",1,utc_timestamp,utc_timestamp);

insert into contrato (nome,data_contrato_inicio,ativo,cadastro,alteracao) values("Construtora X",utc_timestamp,1,utc_timestamp,utc_timestamp);
insert into contrato (nome,data_contrato_inicio,ativo,cadastro,alteracao) values("Construtora Y",utc_timestamp,1,utc_timestamp,utc_timestamp);
insert into contrato (nome,data_contrato_inicio,ativo,cadastro,alteracao) values("Industria Z",utc_timestamp,1,utc_timestamp,utc_timestamp);

insert into usuario_contrato (usuario_id,contrato_id) values (1,1);
insert into usuario_contrato (usuario_id,contrato_id) values (2,1),(2,2);
insert into usuario_contrato (usuario_id,contrato_id) values (3,1),(3,2),(3,3);

insert into item_estoque_tipo (nome,cadastro,alteracao) values ("Ativo Fixo",utc_timestamp,utc_timestamp)
insert into item_estoque_tipo (nome,cadastro,alteracao) values ("Ferramentas",utc_timestamp,utc_timestamp)
insert into item_estoque_tipo (nome,cadastro,alteracao) values ("Consumo",utc_timestamp,utc_timestamp)

insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Veiculo Auto Trator",1,utc_timestamp,utc_timestamp,1,1);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Veiculo Auto Caminhão",1,utc_timestamp,utc_timestamp,1,1);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Veiculo Auto GOL",1,utc_timestamp,utc_timestamp,1,1);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Alicate de Pressão",1,utc_timestamp,utc_timestamp,1,2);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Alicate de Bico",1,utc_timestamp,utc_timestamp,1,2);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Bomba de Sucção",1,utc_timestamp,utc_timestamp,1,1);

insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Veiculo Placa GGT Ford KA",1,utc_timestamp,utc_timestamp,2,1);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Chave de Fenda",1,utc_timestamp,utc_timestamp,2,2);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Mascara",1,utc_timestamp,utc_timestamp,2,3);

insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Truck Ford Placa aaa1234",1,utc_timestamp,utc_timestamp,3,1);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Parafuzadeira Lisa",1,utc_timestamp,utc_timestamp,3,2);
insert into item_estoque (nome,ativo,cadastro,alteracao,contrato_id,item_estoque_tipo_id) values ("Caneta Bic",1,utc_timestamp,utc_timestamp,3,3);

--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("Duas pás",1,"etiqueta 1",1,utc_timestamp,utc_timestamp);
--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("Caçamba dupla",2,"etiqueta 2",1,utc_timestamp,utc_timestamp);
--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("Cor Branca",3,"etiqueta 3",1,utc_timestamp,utc_timestamp);
--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("com mola",4,"etiqueta 4",1,utc_timestamp,utc_timestamp);
--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("sem mola",5,"etiqueta 5",1,utc_timestamp,utc_timestamp);
--insert into estoque (nome,patrimonio_id,etiqueta,ativo,cadastro,alteracao) values ("carburada",6,"etiqueta 6",1,utc_timestamp,utc_timestamp); 

insert into empresa (nome,ativo,cadastro,alteracao) values ('Rogerio Ltda',1,utc_timestamp(),utc_timestamp());
insert into empresa (nome,ativo,cadastro,alteracao) values ('Regina Ltda',1,utc_timestamp(),utc_timestamp());
insert into empresa (nome,ativo,cadastro,alteracao) values ('Leonardo Ltda',1,utc_timestamp(),utc_timestamp());
insert into empresa (nome,ativo,cadastro,alteracao) values ('Isabelle Ltda',1,utc_timestamp(),utc_timestamp());

insert into contrato_empresa (contrato_id,empresa_id) values (1,1);
insert into contrato_empresa (contrato_id,empresa_id) values (1,2);
insert into contrato_empresa (contrato_id,empresa_id) values (2,3);
insert into contrato_empresa (contrato_id,empresa_id) values (3,4);
