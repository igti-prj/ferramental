package br.com.bucksystem.ferramental.domain.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Contrato {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false,length=30,unique=true)
	private String nome;
	
	@Column(columnDefinition = "datetime")
	private OffsetDateTime dataContratoInicio;
	
	@Column(columnDefinition = "boolean default 1")
    private boolean ativo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable( name = "contrato_empresa",
            	joinColumns = @JoinColumn(name = "contrato_id"),
            	inverseJoinColumns = @JoinColumn(name = "empresa_id"))
	private List<Empresa> empresas = new ArrayList<>();
	
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime cadastro;
	
	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime alteracao;
		
}