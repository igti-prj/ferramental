package br.com.bucksystem.ferramental.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bucksystem.ferramental.api.assembler.ItemEstoqueModelAssembler;
import br.com.bucksystem.ferramental.api.assembler.ItemEstoqueModelDisassebler;
import br.com.bucksystem.ferramental.api.dto.ItemEstoqueModel;
import br.com.bucksystem.ferramental.domain.repository.ItemEstoqueRepository;
import br.com.bucksystem.ferramental.domain.service.CadastroItemEstoqueService;

@RestController
@RequestMapping("/api/v1-1/itensestoques")
public class ItemEstoqueApi {

	@Autowired
	private ItemEstoqueRepository itemEstoqueRepository;
	
	@Autowired
	private CadastroItemEstoqueService cadastroItemEstoque;
	
	@Autowired
	private SmartValidator validator;
		
	@Autowired
	private ItemEstoqueModelAssembler itemestoqueModelAssembler;
	
	@Autowired
	private ItemEstoqueModelDisassebler itemestoqueModelDisassembler;
	
	@GetMapping(value="/all" )
	public List<ItemEstoqueModel> listar() {
		return itemestoqueModelAssembler.toCollectionModel(itemEstoqueRepository.findAll());
	}

	@GetMapping(value="/{id}" )
	public ItemEstoqueModel buscarId(@Valid @PathVariable("id") Long pk ) {
		return itemestoqueModelAssembler.toModel(cadastroItemEstoque.buscarOuFalharId(pk));
	}
	
}