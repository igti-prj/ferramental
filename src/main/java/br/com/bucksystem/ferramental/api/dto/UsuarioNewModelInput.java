package br.com.bucksystem.ferramental.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class UsuarioNewModelInput {

	@NotBlank @Size(max=50)
	private String nome;
	@NotBlank @Size(max=100)
	private String senha;
    private boolean ativo;
    private ContratoIdModel contratoId;
	
}