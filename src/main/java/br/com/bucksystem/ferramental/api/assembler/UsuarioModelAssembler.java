package br.com.bucksystem.ferramental.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.UsuarioModel;
import br.com.bucksystem.ferramental.domain.model.Usuario;

@Component
public class UsuarioModelAssembler {
	
	@Autowired
	private ModelMapper modelMapper;
	
	public UsuarioModel toModel(Usuario user) {		
		return modelMapper.map(user, UsuarioModel.class);
	}
	
	public List<UsuarioModel> toCollectionModel(List<Usuario> users) {		
		return users.stream()
				.map(user -> toModel(user))
				.collect(Collectors.toList());
	}

}