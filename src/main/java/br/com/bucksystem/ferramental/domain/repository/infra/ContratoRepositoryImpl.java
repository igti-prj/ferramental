package br.com.bucksystem.ferramental.domain.repository.infra;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import br.com.bucksystem.ferramental.domain.model.Contrato;
import br.com.bucksystem.ferramental.domain.repository.ContratoRepositoryQuery;

@Repository
public class ContratoRepositoryImpl implements ContratoRepositoryQuery {

	@Autowired
	private EntityManager em;
	
	@Override
	public List<Contrato> find(String conteudopesquisa){
		var jpql = new StringBuilder();	
		jpql.append("from Contrato Where 1=1 ");
		
		var parametros = new HashMap<String, Object>();
		
		if ( StringUtils.hasLength(conteudopesquisa)) {
			jpql.append("and nome like :conteudofind");
			parametros.put("conteudofind", "%"+conteudopesquisa+"%");
		}
				
		TypedQuery<Contrato> query = em.createQuery(jpql.toString(), Contrato.class);
		parametros.forEach((chave,valor) -> query.setParameter(chave, valor));
		return query.getResultList();	
	}

}