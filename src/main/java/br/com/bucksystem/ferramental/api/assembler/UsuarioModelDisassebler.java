package br.com.bucksystem.ferramental.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bucksystem.ferramental.api.dto.UsuarioNewModelInput;
import br.com.bucksystem.ferramental.domain.model.Usuario;

@Component
public class UsuarioModelDisassebler {
	
	@Autowired
	private ModelMapper modelMapper;

	public Usuario toDomainObject(UsuarioNewModelInput userInput) {
		return modelMapper.map(userInput, Usuario.class);
	}

	public void copyToDomainObject(UsuarioNewModelInput useInput, Usuario user) {
		modelMapper.map(useInput, user);
	}
}