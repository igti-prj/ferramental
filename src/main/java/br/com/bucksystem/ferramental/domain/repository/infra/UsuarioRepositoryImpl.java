package br.com.bucksystem.ferramental.domain.repository.infra;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import br.com.bucksystem.ferramental.domain.model.Usuario;
import br.com.bucksystem.ferramental.domain.repository.UsuarioRepositoryQuery;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {

	@Autowired
	private EntityManager em;
	
	@Override
	public List<Usuario> find(String conteudopesquisa){
		var jpql = new StringBuilder();	
		jpql.append("from Usuario Where 1=1 ");
		
		var parametros = new HashMap<String, Object>();
		
		if ( StringUtils.hasLength(conteudopesquisa)) {
			jpql.append("and nome like :conteudofind");
			parametros.put("conteudofind", "%"+conteudopesquisa+"%");
		}
				
		TypedQuery<Usuario> query = em.createQuery(jpql.toString(), Usuario.class);
		parametros.forEach((chave,valor) -> query.setParameter(chave, valor));
		return query.getResultList();	
	}

}