package br.com.bucksystem.ferramental.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.bucksystem.ferramental.domain.model.ItemEstoque;
import br.com.bucksystem.ferramental.domain.repository.infra.CustomJpaRepository;

@Repository   
public interface ItemEstoqueRepository 
	extends CustomJpaRepository<ItemEstoque, Long> { 
}