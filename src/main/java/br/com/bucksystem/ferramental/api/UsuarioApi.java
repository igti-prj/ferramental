package br.com.bucksystem.ferramental.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bucksystem.ferramental.api.assembler.UsuarioModelAssembler;
import br.com.bucksystem.ferramental.api.assembler.UsuarioModelDisassebler;
import br.com.bucksystem.ferramental.api.dto.UsuarioModel;
import br.com.bucksystem.ferramental.domain.repository.UsuarioRepository;
import br.com.bucksystem.ferramental.domain.service.CadastroUsuarioService;

@RestController
@RequestMapping("/api/v1-1/usuarios")
public class UsuarioApi {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private CadastroUsuarioService cadastroUsuario;
	
	@Autowired
	private SmartValidator validator;
		
	@Autowired
	private UsuarioModelAssembler usuarioModelAssembler;
	
	@Autowired
	private UsuarioModelDisassebler usuarioModelDisassembler;
	
	@GetMapping(value="/all" )
	public List<UsuarioModel> listar() {
		return usuarioModelAssembler.toCollectionModel(usuarioRepository.findAll());
	}

	@GetMapping(value="/{id}" )
	public UsuarioModel buscarId(@Valid @PathVariable("id") Long pk ) {
		return usuarioModelAssembler.toModel(cadastroUsuario.buscarOuFalharId(pk));
	}
	
	@GetMapping(value="/login/{nomeusuario}" )
	public UsuarioModel buscarEmail(@Valid @PathVariable("nomeusuario") String nome) {
		return usuarioModelAssembler.toModel(cadastroUsuario.buscarOuFalharNome(nome));
	}
	
	@GetMapping(value="/find")
	public List<UsuarioModel> find(String conteudopesquisa) {
		return usuarioModelAssembler.toCollectionModel(usuarioRepository.find(conteudopesquisa));
	}
				
}