package br.com.bucksystem.ferramental.domain.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity
public class Estoque {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false,length=40)
	private String nome;
		
	@Column(nullable=true,length=30)
	private String etiqueta;
	
	private BigDecimal qtdade;
	
	@Column(nullable=false,length=3)
	private String unidadeestoque;
	
	private boolean ativo = true;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Empresa empresa;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private ItemEstoque itemEstoque;	

	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime cadastro;
	
	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime alteracao;
	
}