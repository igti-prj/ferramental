package br.com.bucksystem.ferramental.api.dto;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class ContratoModel {

	private Long id;
	private String nome;
	private OffsetDateTime dataContratoInicio;
    private boolean ativo;
    
}
