package br.com.bucksystem.ferramental.api.dto;

import java.util.List;

import lombok.Data;

@Data
public class UsuarioModel {

	private Long id;
	private String nome;
	private String senha;
    private boolean ativo;
    private List<ContratoModel> contratos;
	
}