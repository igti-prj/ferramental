package br.com.bucksystem.ferramental.api.dto;

import lombok.Data;

@Data
public class EstoqueModel {

	private Long id;
	private String nome;
	private String etiqueta;
	private boolean ativo;

}
