package br.com.bucksystem.ferramental.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.bucksystem.ferramental.domain.model.Estoque;
import br.com.bucksystem.ferramental.domain.repository.infra.CustomJpaRepository;

@Repository   
public interface EstoqueRepository 
	extends CustomJpaRepository<Estoque, Long> { 
}